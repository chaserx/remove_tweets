# Remove Tweets

This python script will sequentially delete all of the tweets in the specified account.

## Requirements 

- Python 3
- Twitter account
- [Twitter developer account](https://developer.twitter.com/en/support/twitter-api/developer-account)
- Twitter Project and App

## Before you begin

⚠️ This is a destructive, irreversible process. ⚠️

Download [an archive of your twitter data](https://help.twitter.com/en/managing-your-account/accessing-your-twitter-data). This can be slow, plan ahead. 

## Getting started

Install python dependencies 

1. `pip3 install -r requirements.txt`
2. `cp .env.example .env`
3. Replace values in `.env` with values from twitter project and app

## Usage

Tweet deletion is performed in batches of 49 every 15 min to comply with rate limits. Delete batch and wait, repeat. Be prepared to let this process run for a long time. Too many requests or requests too quickly will result in a HTTP 429 code returned from the Twitter API. 

Run `python3 ./remove_all_tweets.py` to begin. 