#!/usr/bin/python3

import tweepy
import time
import os
from tqdm import tqdm
from dotenv import load_dotenv, find_dotenv

class TwitterClient:

    def connectAPI(self):
        self.client = tweepy.Client(bearer_token=os.environ.get("BEARER_TOKEN"),
                                    consumer_key=os.environ.get("API_KEY"), 
                                    consumer_secret=os.environ.get("API_SECRET"),
                                    access_token=os.environ.get("ACCESS_TOKEN"),
                                    access_token_secret=os.environ.get("ACCESS_TOKEN_SECRET"), 
                                    wait_on_rate_limit=True)
        me = self.client.get_me()
        print(f"account: {me.data}")
        self.user_id = me.data.id

    # per the twitter api docs, rate limit is 50 requests per 15-minute window
    # https://developer.twitter.com/en/docs/twitter-api/tweets/manage-tweets/api-reference/delete-tweets-id
    #
    def delete_tweets_per_page(self, per_page=49):
        try:
            for tweet in tweepy.Paginator(self.client.get_users_tweets, self.user_id, max_results=per_page):
                if tweet.data is not None:
                    for tweet in tweet.data:
                        print(f"- deleting tweet {tweet.id} {repr(tweet.text)}")
                        self.client.delete_tweet(tweet.id)
                        time.sleep(1)
                    self.countdown(900) #15 min timeout to conform to twitter API limits for the number of deletions
        except tweepy.errors.TweepyException:
          raise
    
    def countdown(self, number):
        pbar = tqdm(range(number,0,-1))
        for n in pbar:
            time.sleep(1)
            pbar.set_description("Waiting %s seconds" % n)
        

def main():
    load_dotenv(find_dotenv())  # take environment variables from .env file
    twitter_client = TwitterClient()
    twitter_client.connectAPI()
    twitter_client.delete_tweets_per_page()

main()
